from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList

# Create your views here.


def todoslist_view(request):
    todoslist_list = TodoList.objects.all()
    context = {
        "todoslist_list": todoslist_list
    }
    return render(request, "todos/list.html", context)


def todoslist_detail(request, id):
    todoslist_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todoslist_detail": todoslist_detail,
    }
    return render(request, "todos/detail.html", context)
