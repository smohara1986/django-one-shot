from django.urls import path
from todos.views import todoslist_view, todoslist_detail


urlpatterns = [
    path("", todoslist_view, name="todo_list_list"),
    path("<int:id>/", todoslist_detail, name="todo_list_detail")
]
